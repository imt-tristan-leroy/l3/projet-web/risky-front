# Zenko's Nextjs Frontend template (to be replaced with <my_project> name)

## Description

Made with: [Next.js](https://nextjs.org/)

Frontend

## Project Resources

* [**Graphql Schema**](docs/schemas)

## How to use this template

```bash
# Clone the repository
git clone git@gitlab.com:zenko-templates/nodejs/ts/nextjs-ts-template.git <my_project>

# Delete the git template's data
cd <my_project>
rm -rf .git
```

Then configure your new project accordingly

### Update Api name and version

* [ ] `src/pages/_app.tsx`: change `title`` and `description` of the page
* [ ] `public/favicon.ico`: change the favicon
* [ ] `package.json`: change `name`, `version` and `description` attributes

### Finalize bootstrap

```bash
# Initialize your new repository
git init
git remote add origin git@gitlab.com:<my_project>.git
git commit -a -m "Initial commit"
git push -u origin master
```

## Installation

### Requirements

* [NodeJS v17.4.0+](https://nodejs.org/en/blog/release/v17.4.0/)
* [PNPM v7](https://pnpm.io)

**Use a Node environment manager like [NVM](https://github.com/nvm-sh/nvm) to quickly change Node/Yarn version.**

### Environment variables

First copy `.env` to provide local `.env.local` file

```bash
$ cp .env.local .env
```

Update environment variables values if necessary

### Install

```bash
$ pnpm i
```

### Generate graphql typing

```bash
$ pnpm generate:graphql
```

### Running the app

```bash
# Development build
$ pnpm dev

# Production build
$ pnpm build
$ pnpm start
```
