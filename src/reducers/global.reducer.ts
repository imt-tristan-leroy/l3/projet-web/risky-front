import  {createSlice, PayloadAction} from '@reduxjs/toolkit';

export type GlobalState = {
    isAuthenticated : boolean
};

const initialState: GlobalState = {
    isAuthenticated: false
};

export const globalSlice = createSlice ({
    name: 'global',
    initialState,
    reducers: {setAuthenticated: (state, action: PayloadAction<boolean>) => {
        state.isAuthenticated = action.payload;
    }}
})

export const { setAuthenticated } = globalSlice.actions

export default globalSlice.reducer;