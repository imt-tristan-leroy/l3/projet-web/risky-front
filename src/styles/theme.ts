import { createTheme } from '@mui/material/styles';

declare module '@mui/material/styles' {
	interface Theme {

	}
	// allow configuration using `createTheme`
	interface ThemeOptions {
		
	}
}

// Create a theme instance.
const theme = createTheme({
	palette: {
		primary: {
			main: '#4f4f4b',
			dark: '#7e2c2d',
			light: '#fdffbf',
		  },
		  secondary: {
			main: '#1a1a05',
			dark: '#ab4300',
			light: '#f78033',
		  },
	  },
});

export default theme;
