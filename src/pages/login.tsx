import type { NextPage } from 'next'
import styles from '../styles/Home.module.css'
import {usePostAuthLoginMutation} from "../api/riskyApi";
import {useState, useEffect} from "react";
import * as React from 'react';

import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import LoginIcon from '@mui/icons-material/Login';
import { useDispatch } from 'react-redux';
import { setAuthenticated } from '../reducers/global.reducer';
import Box from '@mui/material/Box';
import { useRouter } from 'next/router';


const Login: NextPage = () => {

  const [postLogin, data] = usePostAuthLoginMutation();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const router = useRouter()
  
  useEffect(() => {
    dispatch(setAuthenticated(data.isSuccess));
    if (data.isSuccess) {
      router.push("/")
    }
}, [data])

  const login = () => {
    postLogin({ body: {email, password}})
  };

  const handleChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  };

  const handleChangePassword = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };


  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <h1>
          Login
        </h1>
        
      <TextField
        id="outlined-name"
        label="Email"
        margin='dense'
        value={email}
        onChange={handleChangeEmail}
      />
      <TextField
        id="outlined-password"
        label="Password"
        type="password"
        margin='dense'
        value={password}
        onChange={handleChangePassword}
      />
      <Button variant="outlined" onClick={login} startIcon={<LoginIcon />}>
        Login
      </Button>
      <Box sx={{ '& button': { m: 1 } }}>
        Dont'have an account yet ?
      <Button href="/register">
        Sign up
      </Button>
      </Box>
      </main>
    </div>
  )
}

export default Login
