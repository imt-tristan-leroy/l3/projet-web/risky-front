import type { NextPage } from 'next'
import styles from '../styles/Home.module.css'

const Home: NextPage = () => {

  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to Risky Game !
        </h1>

        <p className={styles.description}>
          Get started by choosing an option below! Join the adventure!
        </p>

        <div className={styles.grid}>
          <a href="https://bitbucket.org/imt-mobisyst/hackagames/src/master/" className={styles.card}>
            <h2> HackaGames &rarr;</h2>
            <p>Official Bitbucket of HackaGames</p>
          </a>

          <a href="/game" className={styles.card}>
            <h2>The Game &rarr;</h2>
            <p>Click here to start a new game</p>
          </a>

          <a
            href="/history"
            className={styles.card}
          >
            <h2>Match History &rarr;</h2>
            <p>Here you can see the match history</p>
          </a>

          <a
            href="/ia"
            className={styles.card}
          >
            <h2>Add New AI &rarr;</h2>
            <p>
            Add your AI! So it can fight against all the others
            </p>
          </a>
        </div>
      </main>

    </div>
  )
}

export default Home
