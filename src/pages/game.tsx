import type { NextPage } from 'next'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import TextField from '@mui/material/TextField';
import {useState} from "react";
import Grid from '@mui/material/Grid';
import { Divider, Typography } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import { useGetIaQuery, usePostGamesStartMutation } from '../api/riskyApi';
import { Box } from '@mui/system';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Backdrop from '@mui/material/Backdrop';
import { useRouter } from 'next/router'


const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};

const Game: NextPage = () => {
  const [userId, setUserId] = useState("");
  const [aiPlayer1, setAiPlayer1] = useState("");
  const [aiPlayer2, setAiPlayer2] = useState("");
  const [human, setHuman] = useState(false);
  const {data: dataAi} = useGetIaQuery();
  const [postGamesStart, gameResult] = usePostGamesStartMutation();
  const [open, setOpen] = useState(false);
  const router = useRouter();

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);



  const handleChange1 = (event: SelectChangeEvent) => {
    setAiPlayer1(event.target.value);
  };

  const handleChange2 = (event: SelectChangeEvent) => {
    setAiPlayer2(event.target.value);
  };

  const handleChange3 = (event: React.ChangeEvent<HTMLInputElement>) => {
    setHuman(event.target.checked);
  };

function aiGameStart(ai1 : string, ai2 : string) {
  postGamesStart({body:{ia1Id : ai1, ia2Id : ai2}});
  handleOpen();
}

function humanGameStart(userid: string ,ai : string) {
//TODO : GET ME (add user id)
  postGamesStart({body:{userId : userid, ia2Id : ai}});
  router.push("/playgame");
}

  return (
    <Box style={{ height: "82.5vh"}}>
      <Box sx={{ml : 10, m: 5, mx: "auto"}}>
        <h1>Select Players!</h1>
      </Box>
      <Box  sx={{ ml : 10, m : 5, mx: "auto"}}>
        <Grid container spacing={10}>
          
          <Grid item xs={6}>
            
            <h2>Player 1</h2>
            
            <Checkbox onChange={handleChange3}/>Human Player
            {!human?(
              <div>
                <FormControl fullWidth>
                <InputLabel id="AI Player 1">Choose AI</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={aiPlayer1}
                  label="Choose the AI"
                  onChange={handleChange1}
                >
                  {dataAi && dataAi.map((ia) => (
                    <MenuItem key={ia.id} value={ia.id}>{ia.name}</MenuItem>
                  ))}
                </Select>
              </FormControl>
              <Box sx={{ m : 3, mx: "auto"}}>
              <TextField id="standard-basic" variant="standard" disabled={true} label="Number of Games"/>
                <Button sx={{m:3}} variant="contained" onClick={() => aiGameStart(aiPlayer1, aiPlayer2)} >
                Start AI game!
                </Button>
              </Box>
            </div>
            ) : (
              <Box sx={{ m : 12.7, mx: "auto"}}>
                <Button  variant="contained"  onClick={() => humanGameStart(userId, aiPlayer2)} >
                Start Human game!
                </Button>
              </Box>
            )}          
            
          </Grid>
          
          <Grid item xs={6} sx={{ pr: 10 }}>
              <h2>Player 2 - AI</h2>
              <Box sx={{ m : 7, mx: "auto"}}>
              <FormControl fullWidth>
                  <InputLabel id="AI Player 2">Choose AI</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={aiPlayer2}
                    label="Choose the AI"
                    onChange={handleChange2}
                  >
                  {dataAi && dataAi.map((ia) => (
                    <MenuItem key={ia.id} value={ia.id}>{ia.name}</MenuItem>
                  ))}
                  </Select>
                </FormControl>
              </Box>
            </Grid>
        </Grid>
      </Box>
      {gameResult && gameResult.data && gameResult.data.end && (
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Typography id="transition-modal-title" variant="h6" component="h2">
              {gameResult.data.end.player1? "Player 1" : "Player 2"} won the game!
            </Typography>

          </Box>
        </Fade>
      </Modal>
      )}
    </Box>
  )
}

export default Game