import type { NextPage } from 'next'
import {usePostGamesMoveMutation, usePostGamesSleepMutation, usePostGamesGrowMutation} from "../api/riskyApi";
import {useState, useEffect} from "react";
import * as React from 'react';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import AccessibilityIcon from '@mui/icons-material/Accessibility';
import AccessibilityNewIcon from '@mui/icons-material/AccessibilityNew';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import FormLabel from '@mui/material/FormLabel';
import DoubleArrowIcon from '@mui/icons-material/DoubleArrow';
import BedtimeIcon from '@mui/icons-material/Bedtime'
import Grid from '@mui/material/Grid';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select, { SelectChangeEvent} from '@mui/material/Select';
import { TaskAlt } from '@mui/icons-material';
import TextField from '@mui/material/TextField';


const Playgame: NextPage = () => {

  const [postGrow, dataGrow] = usePostGamesGrowMutation();
  const [postMove, dataMove] = usePostGamesMoveMutation();
  const [postSleep, dataSleep] = usePostGamesSleepMutation();
  const [action, setAction] = useState("");


  const handleChangeAction = (event: React.ChangeEvent<HTMLInputElement>) => {
    setAction((event.target as HTMLInputElement).value);
  };

  const [node, setGrow] = React.useState('');
  const [nodeIni, setMoveIni] = React.useState('');
  const [nodeEnd, setMoveEnd] = React.useState('');
  const [Qty, setMoveQty] = React.useState<number>();

  const handleChangeGrow = (event: SelectChangeEvent) => {
    setGrow(event.target.value as string);
  };
  const handleChangeMoveIni = (event: SelectChangeEvent) => {
    setMoveIni(event.target.value as string);
  };
  const handleChangeMoveEnd = (event: SelectChangeEvent) => {
    setMoveEnd(event.target.value as string);
  };
  const handleChangeMoveQty = (event: React.ChangeEvent<HTMLInputElement>) => {
    setMoveQty(+event.target.value);
  };

  const confirm = () => {
    if(action === 'grow') postGrow({ body: {node}})
    if(action === 'move') postMove({ body: {nodeIni, nodeEnd, Qty}})
    if(action === 'sleep') postSleep()
    setGrow("")
    setMoveIni("")
    setMoveEnd("")
    setMoveQty(+"")
  };

  return (
      <Grid container alignItems="center" columns={10} sx={{mt:2}}>
        <img src="/risky-screenshot.png" height={450} style={{ margin: 'auto'}}/>
      <Grid item container spacing={2} alignItems="center" style={{ margin: 'auto'}}>
        <Grid item xs={2}>
          <FormControl sx={{ml : "5rem"}}>
          <FormLabel id="demo-radio-buttons-group-label">Action</FormLabel>
          <RadioGroup
            aria-labelledby="demo-radio-buttons-group-label"
            value={action}
            name="radio-buttons-group"
            onChange={handleChangeAction}
          >
            <FormControlLabel 
            value="grow"
            control={
            <Radio 
            icon={<AccessibilityIcon fontSize="small"/>}
            checkedIcon={<AccessibilityNewIcon fontSize="large"/>}
            />} label="Grow" />
            <FormControlLabel 
            value="move"
            control={
            <Radio 
            icon={<DoubleArrowIcon fontSize="small"/>}
            checkedIcon={<DoubleArrowIcon fontSize="large"/>}
            />} label="Move" />
            <FormControlLabel 
            value="sleep"
            control={
            <Radio 
            icon={<BedtimeIcon fontSize="small"/>}
            checkedIcon={<BedtimeIcon fontSize="large"/>}
            />} label="Sleep" />
          </RadioGroup>
          </FormControl>
        </Grid>   
        <Grid item xs={8} style={{ margin: 'auto'}} id="su">
           {action === "grow" && (
            <Box sx={{ minWidth: 120 }}>
              <FormControl fullWidth>
                <InputLabel id="selectGrowLabel">Cell</InputLabel>
                <Select 
                  labelId="selectGrowLabel"
                  id="selectGrow"
                  value={node}
                  label="Node"
                  onChange={handleChangeGrow}
                >
                  <MenuItem value={0}>Cell-00</MenuItem>
                  <MenuItem value={1}>Cell-01</MenuItem>
                  <MenuItem value={2}>Cell-02</MenuItem>
                  <MenuItem value={3}>Cell-03</MenuItem>
                  <MenuItem value={4}>Cell-04</MenuItem>
                  <MenuItem value={5}>Cell-05</MenuItem>
                  <MenuItem value={6}>Cell-06</MenuItem>
                  <MenuItem value={7}>Cell-07</MenuItem>
                  <MenuItem value={8}>Cell-08</MenuItem>
                  <MenuItem value={9}>Cell-09</MenuItem>
                  <MenuItem value={10}>Cell-10</MenuItem>
                  <MenuItem value={11}>Cell-11</MenuItem>
                  <MenuItem value={12}>Cell-12</MenuItem>
                  <MenuItem value={13}>Cell-13</MenuItem>

                </Select>
              </FormControl>
            </Box>
           )}
           {action === "move" && (
             <Grid item container spacing={2}>
               <Grid item xs={4} >
                  <FormControl fullWidth>
                    <InputLabel id="selectMoveIniLabel">Cell Start</InputLabel>
                    <Select 
                      labelId="selectMoveIniLabel"
                      id="selectMoveIni"
                      value={nodeIni}
                      label="NodeIni"
                      onChange={handleChangeMoveIni}
                      autoWidth={true}
                    >
                     <MenuItem value={0}>Cell-00</MenuItem>
                     <MenuItem value={1}>Cell-01</MenuItem>
                     <MenuItem value={2}>Cell-02</MenuItem>
                     <MenuItem value={3}>Cell-03</MenuItem>
                     <MenuItem value={4}>Cell-04</MenuItem>
                     <MenuItem value={5}>Cell-05</MenuItem>
                     <MenuItem value={6}>Cell-06</MenuItem>
                     <MenuItem value={7}>Cell-07</MenuItem>
                     <MenuItem value={8}>Cell-08</MenuItem>
                     <MenuItem value={9}>Cell-09</MenuItem>
                     <MenuItem value={10}>Cell-10</MenuItem>
                     <MenuItem value={11}>Cell-11</MenuItem>
                     <MenuItem value={12}>Cell-12</MenuItem>
                     <MenuItem value={13}>Cell-13</MenuItem>

                    </Select>
                  </FormControl>
                </Grid> 
                <Grid item xs={4} >
                  <FormControl fullWidth>
                    <InputLabel id="selectMoveEndLabel">Cell End</InputLabel>
                    <Select 
                      labelId="selectMoveEndLabel"
                      id="selectMoveEnd"
                      value={nodeEnd}
                      label="NodeEnd"
                      onChange={handleChangeMoveEnd}
                      autoWidth={true}
                    >

                     <MenuItem value={0}>Cell-00</MenuItem>
                     <MenuItem value={1}>Cell-01</MenuItem>
                     <MenuItem value={2}>Cell-02</MenuItem>
                     <MenuItem value={3}>Cell-03</MenuItem>
                     <MenuItem value={4}>Cell-04</MenuItem>
                     <MenuItem value={5}>Cell-05</MenuItem>
                     <MenuItem value={6}>Cell-06</MenuItem>
                     <MenuItem value={7}>Cell-07</MenuItem>
                     <MenuItem value={8}>Cell-08</MenuItem>
                     <MenuItem value={9}>Cell-09</MenuItem>
                     <MenuItem value={10}>Cell-10</MenuItem>
                     <MenuItem value={11}>Cell-11</MenuItem>
                     <MenuItem value={12}>Cell-12</MenuItem>
                     <MenuItem value={13}>Cell-13</MenuItem>

                    </Select>
                  </FormControl>
                </Grid> 
                <Grid item xs={4} >
                  <FormControl fullWidth>
                    <InputLabel id="selectMoveQtyLabel"></InputLabel>
                    <TextField
                      id="selectMoveQty"
                      label="Strength"
                      type="number"
                      value={Qty}
                      onChange={handleChangeMoveQty}
                      defaultValue = '0'
                      InputProps={{ inputProps: { min: 0 } }}
                      InputLabelProps={{
                        shrink: true,
                        }}
                      />
                  </FormControl>
                </Grid> 
             </Grid>
           )}
           {action === "sleep" && (
            "Reset the action counter to 0 for all the cells ?"
           )}
        
        </Grid>   
        <Grid item xs={2} >
        <Button variant="outlined" onClick={confirm} startIcon={<TaskAlt />} disabled={action !== 'sleep' && action !== 'grow' && action !== 'move'} >
            Confirm
        </Button>
        </Grid>  
      </Grid>
      </Grid>
  )
}

export default Playgame
