import type { NextPage } from 'next'
import styles from '../styles/Home.module.css'
import * as React from 'react';


import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { useGetIaQuery, usePostIaMutation } from '../api/riskyApi';
import { Button, Modal, TextField, Fade, Box, Backdrop, Grid } from '@mui/material';
import MonacoEditor from '@monaco-editor/react'



const Ia: NextPage = () => {

  const { data : ia, isLoading, isFetching, isError } = useGetIaQuery();

  interface Column {
    id: 'name' | 'action';
    label: string;
    minWidth?: number;
    align?: 'right';
    format?: (value: number) => string;
  }

  const columns: readonly Column[] = [
    { id: 'name', label: 'Name', minWidth: 170 },
    { id: 'action', label: 'Action', minWidth: 170 },
  ];

  const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

  const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 800,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
  };

  const [open, setOpen] = React.useState(false);
const [name, setName] = React.useState("");
const [script, setScript] = React.useState("");
const [addIa, data] = usePostIaMutation();
const handleAddIa = () => {
  addIa({body: {name, script}}); 
  handleClose();
}

const handleOpen = () => {
  setOpen(true);
}

const handleClose = () => {
  setOpen(false);
  setName("")
  setScript("") 
}

const handleChangeName = (event: React.ChangeEvent<HTMLInputElement>) => {
  setName(event.target.value);
};

const handleScriptChange = (s: string | undefined) => {
  setScript(s ?? "");
}



  return (
    <div className={styles.container}>
      <Button onClick={handleOpen}>Add</Button>
      <Paper sx={{ width: '100%', overflow: 'hidden' }}>
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {ia && ia.map((row) => {
                return (
                  // eslint-disable-next-line react/jsx-key
                  <TableRow hover role="checkbox" tabIndex={-1} >
                    <TableCell>
                      {row['name']}
                    </TableCell>
                    <TableCell>
                      <Button disabled={true}>Edit</Button>
                    </TableCell>
                  </TableRow>
                );
              })} 
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
    <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
          <Grid container spacing={2}>
          <Grid item xs={6}>
            <h2>Add IA</h2>
            </Grid>
            <Grid item xs={6}>
            <TextField label="Name" variant="filled" color="success" focused value={name} onChange={handleChangeName}/>
            </Grid>
            <Grid item xs={12}>
            <MonacoEditor
            height="25vh"
            language="python"
            value={script}
            onChange={handleScriptChange}
            />
            </Grid>
            <Grid item xs={12}>
            <Button onClick={handleAddIa}>Submit</Button>  
            </Grid>
            </Grid>
          </Box>
          </Fade>                   
        </Modal>
    </div>
  )
}

export default Ia
