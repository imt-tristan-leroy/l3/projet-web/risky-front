import type { NextPage } from 'next'
import styles from '../styles/Home.module.css'
import {useGetHistoryQuery} from "../api/riskyApi";
import * as React from 'react';


import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { Box } from '@mui/system';



const History: NextPage = () => {

  const { data : history, isLoading, isFetching, isError } = useGetHistoryQuery({});

  interface Column {
    id: 'player1' | 'player2' | 'gameid' | 'winner' | 'date';
    label: string;
    minWidth?: number;
    align?: 'right';
    format?: (value: number) => string;
  }

  const columns: readonly Column[] = [
    { id: 'player1', label: 'Player 1', minWidth: 170 },
    { id: 'player2', label: 'Player 2', minWidth: 170 },
    { id: 'winner', label: 'Winner', minWidth: 170 },
    { id: 'date', label: 'Date', minWidth: 170,},
  ];

  return (
    <div className={styles.container}>
      <Box sx={{ textAlign : "center" , mt: "4rem"}}>
      <h1>Match History</h1>
      </Box>
      <main className={styles.main}>
      <Paper sx={{ width: '100%', overflow: 'hidden' , mt : "-12rem"}}>
      <TableContainer sx={{ maxHeight: 440}}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {history && history.map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                  <TableCell>
                    {row.userId && row.userId !== "00000000-0000-0000-0000-000000000000" ? row.userId + ' (User)': row.ia1Id + ' (IA)'}
                  </TableCell>
                    <TableCell>
                      {row.ia2Id} (IA)
                    </TableCell>
                    <TableCell>
                      {row.winner}
                    </TableCell>
                    <TableCell>
                      {row.date}
                    </TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
      </main>
    </div>
  )
}

export default History
