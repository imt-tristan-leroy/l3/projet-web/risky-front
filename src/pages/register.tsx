import type { NextPage } from 'next'
import styles from '../styles/Home.module.css'
import {usePostAuthRegisterMutation} from "../api/riskyApi";
import {useState, useEffect} from "react";
import * as React from 'react';

import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import CreateIcon from '@mui/icons-material/Create';
import { useRouter } from 'next/router';



const Register: NextPage = () => {

  const [postRegister, data] = usePostAuthRegisterMutation();
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const router = useRouter()

  useEffect(() => {
    if(data.isSuccess) router.push("/login")
}, [data])

  const register = () => {
    postRegister({ body: {username, email, password}})
  };

  const handleChangeEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  };

  const handleChangeUsername = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(event.target.value);
  };

  const handleChangePassword = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };


  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <h1>
          Register
        </h1>
        <TextField
        id="outlined-name"
        label="Email"
        margin='dense'
        value={email}
        onChange={handleChangeEmail}
      />  
      <TextField
        id="outlined-name"
        label="Username"
        margin='dense'
        value={username}
        onChange={handleChangeUsername}
      />
      <TextField
        id="outlined-password"
        label="Password"
        type="password"
        margin='dense'
        value={password}
        onChange={handleChangePassword}
      />
      <Button variant="outlined" onClick={register} startIcon={<CreateIcon />} >
        Register
      </Button>
      
      </main>
    </div>
  )
}

export default Register
