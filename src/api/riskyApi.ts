import { emptySplitApi as api } from "./emptyApi";
export const injectedRtkApi = api.injectEndpoints({
  endpoints: (build) => ({
    getIa: build.query<GetIaApiResponse, GetIaApiArg>({
      query: () => ({ url: `/ia` }),
      providesTags: ["ia"]
    }),
    postIa: build.mutation<PostIaApiResponse, PostIaApiArg>({
      query: (queryArg) => ({
        url: `/ia`,
        method: "POST",
        body: queryArg.body,
      }),
      invalidatesTags: ["ia"]
    }),
    getIaById: build.query<GetIaByIdApiResponse, GetIaByIdApiArg>({
      query: (queryArg) => ({ url: `/ia/${queryArg.id}` }),
      providesTags: ["ia"]
    }),
    putIaById: build.mutation<PutIaByIdApiResponse, PutIaByIdApiArg>({
      query: (queryArg) => ({
        url: `/ia/${queryArg.id}`,
        method: "PUT",
        body: queryArg.body,
      }),
      invalidatesTags: ["ia"]
    }),
    postGamesMove: build.mutation<
      PostGamesMoveApiResponse,
      PostGamesMoveApiArg
    >({
      query: (queryArg) => ({
        url: `/games/_move`,
        method: "POST",
        body: queryArg.body,
      }),
      invalidatesTags: ["game", "history"]
    }),
    postGamesSleep: build.mutation<
      PostGamesSleepApiResponse,
      PostGamesSleepApiArg
    >({
      query: () => ({ url: `/games/_sleep`, method: "POST" }),
      invalidatesTags: ["game", "history"]
    }),
    postGamesGrow: build.mutation<
      PostGamesGrowApiResponse,
      PostGamesGrowApiArg
    >({
      query: (queryArg) => ({
        url: `/games/_grow`,
        method: "POST",
        body: queryArg.body,
      }),
      invalidatesTags: ["game", "history"]
    }),
    getHistory: build.query<GetHistoryApiResponse, GetHistoryApiArg>({
      query: (queryArg) => ({
        url: `/history/`,
        params: { "ia-id": queryArg["ia-id"] },
      }),
      providesTags: ["history"]
    }),
    postGamesStart: build.mutation<
      PostGamesStartApiResponse,
      PostGamesStartApiArg
    >({
      query: (queryArg) => ({
        url: `/games/_start`,
        method: "POST",
        body: queryArg.body,
      }),
      invalidatesTags: ["history"]
    }),
    postAuthLogin: build.mutation<
      PostAuthLoginApiResponse,
      PostAuthLoginApiArg
    >({
      query: (queryArg) => ({
        url: `/auth/login`,
        method: "POST",
        body: queryArg.body,
      }),
    }),
    postAuthRegister: build.mutation<
      PostAuthRegisterApiResponse,
      PostAuthRegisterApiArg
    >({
      query: (queryArg) => ({
        url: `/auth/register`,
        method: "POST",
        body: queryArg.body,
      }),
    }),
    postAuthToken: build.mutation<
      PostAuthTokenApiResponse,
      PostAuthTokenApiArg
    >({
      query: (queryArg) => ({
        url: `/auth/token`,
        method: "POST",
        cookies: { refreshToken: queryArg.refreshToken },
      }),
    }),
  }),
  overrideExisting: false,
});
export { injectedRtkApi as riskyApi };
export type GetIaApiResponse = /** status 200 OK */ Ia[];
export type GetIaApiArg = void;
export type PostIaApiResponse = /** status 200 OK */ Ia;
export type PostIaApiArg = {
  body: {
    name?: string;
    script?: string;
    ""?: string;
  };
};
export type GetIaByIdApiResponse = /** status 200 OK */ Ia;
export type GetIaByIdApiArg = {
  id: string;
};
export type PutIaByIdApiResponse = /** status 200 OK */ Ia;
export type PutIaByIdApiArg = {
  id: string;
  body: {
    Nom?: string;
  };
};
export type PostGamesMoveApiResponse = /** status 200 OK */ GameResult;
export type PostGamesMoveApiArg = {
  body: {
    nodeIni?: string;
    nodeEnd?: string;
    Qty?: number;
  };
};
export type PostGamesSleepApiResponse = /** status 200 OK */ GameResult;
export type PostGamesSleepApiArg = void;
export type PostGamesGrowApiResponse = /** status 200 OK */ GameResult;
export type PostGamesGrowApiArg = {
  body: {
    node?: string;
  };
};
export type GetHistoryApiResponse = /** status 200 OK */ {
  id: string;
  userId?: string;
  ia1Id?: string;
  ia2Id: string;
  date: string;
  winner: string;
}[];
export type GetHistoryApiArg = {
  "ia-id"?: string;
};
export type PostGamesStartApiResponse = /** status 200 OK */ {
  end?: {
    player1: boolean;
    player2: boolean;
  };
};
export type PostGamesStartApiArg = {
  body: {
    userId?: string;
    ia1Id?: string;
    ia2Id: string;
  };
};
export type PostAuthLoginApiResponse = /** status 200 OK */ {
  accessToken?: string;
  refreshToken?: string;
};
export type PostAuthLoginApiArg = {
  body: {
    email?: string;
    password?: string;
  };
};
export type PostAuthRegisterApiResponse = /** status 200 OK */ {
  accessToken?: string;
  refreshToken?: string;
};
export type PostAuthRegisterApiArg = {
  body: {
    username?: string;
    password?: string;
    email?: string;
  };
};
export type PostAuthTokenApiResponse = /** status 200 OK */ {
  accessToken?: string;
  refreshToken?: string;
};
export type PostAuthTokenApiArg = {
  refreshToken?: string;
};
export type Ia = {
  id?: string;
  name?: string;
  script?: string;
};
export type GameResult = {
  pieces: {
    location?: number;
    player?: number;
    quantity?: number;
    lock?: boolean;
  }[];
  game: {
    remaining_turn?: number;
    occupied_nodes?: number;
  };
  result?: boolean;
};
export const {
  useGetIaQuery,
  usePostIaMutation,
  useGetIaByIdQuery,
  usePutIaByIdMutation,
  usePostGamesMoveMutation,
  usePostGamesSleepMutation,
  usePostGamesGrowMutation,
  useGetHistoryQuery,
  usePostGamesStartMutation,
  usePostAuthLoginMutation,
  usePostAuthRegisterMutation,
  usePostAuthTokenMutation,
} = injectedRtkApi;
