import {combineReducers, configureStore} from '@reduxjs/toolkit'
import { setupListeners } from '@reduxjs/toolkit/query'
import { injectedRtkApi } from "../api/riskyApi";
import globalReducer from '../reducers/global.reducer';


const rootReducer = combineReducers({
	// You can add reducer here
	[injectedRtkApi.reducerPath]: injectedRtkApi.reducer,
	global: globalReducer
});

export const store = configureStore({
	reducer: rootReducer,
	middleware: getDefaultMiddleware => [
		// You can add middleware here for async method
		...getDefaultMiddleware(),
		injectedRtkApi.middleware,
	],
});

export type RootState = ReturnType<typeof store.getState>;

setupListeners(store.dispatch);