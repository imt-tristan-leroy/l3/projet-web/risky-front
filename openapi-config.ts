import type { ConfigFile } from '@rtk-query/codegen-openapi'

const config: ConfigFile = {
	schemaFile: 'docs/API.yaml',
	apiFile: './src/api/emptyApi.ts',
	apiImport: 'emptySplitApi',
	outputFile: './src/api/riskyApi.ts',
	exportName: 'riskyApi',
	hooks: true,
}

export default config